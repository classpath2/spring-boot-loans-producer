package com.synechron.loansproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoansProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoansProducerApplication.class, args);
    }

}
