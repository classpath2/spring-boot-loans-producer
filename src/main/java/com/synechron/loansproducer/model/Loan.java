package com.synechron.loansproducer.model;

import lombok.*;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Loan {
    private long loanId;
    private double amount;
    private String customerName;
}