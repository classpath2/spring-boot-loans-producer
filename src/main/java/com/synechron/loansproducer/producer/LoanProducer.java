package com.synechron.loansproducer.producer;

import com.synechron.loansproducer.model.Loan;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import com.github.javafaker.Faker;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Slf4j
public class LoanProducer {

    private final KafkaTemplate<Long, Loan> kafkaTemplate;
    private final Faker faker = new Faker();

    @EventListener

    public void onApplicationEvent(ApplicationReadyEvent event) {
        IntStream.range(1,250).forEach(index ->  {
            Loan loan = Loan.builder()
                                .loanId(faker.number().randomNumber())
                                .amount(faker.number().randomDouble(2, 25000, 30000))
                                .customerName(faker.name().firstName())
                        .build();
            ProducerRecord<Long, Loan> record = new ProducerRecord<>("customer-loans-topic", faker.number().randomNumber(), loan);
            log.info("Sending record to topic :: "+ loan);
            kafkaTemplate.send(record);
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }
}